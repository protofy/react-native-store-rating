import { ViewStyle } from "react-native";

export interface IProps {
	modalTitle: string;
	cancelBtnText: string;
	totalStarCount: number;
	defaultStars: number;
	emptyCommentErrorMessage: string;
	isVisible: boolean;
	isModalOpen: boolean;
	commentPlaceholderText: string;
	rateBtnText: string;
	sendBtnText: string;
	storeRedirectThreshold: number;
	onStarSelected: (e: number) => void;
	onClosed: (cancelled?: boolean) => void;
	sendContactUsForm: (state: IState) => void;
	onSubmitRating: (e: number) => void;
	playStoreUrl?: string;
	iTunesStoreUrl?: string;
	style?: ViewStyle;
	starLabels: string[];
	isTransparent: boolean;
	buttonContainerStyle?: ViewStyle;
	buttonStyle: ViewStyle;
	buttonTextStyle?: ViewStyle;
	buttonCancelStyle?: ViewStyle;
	buttonCancelTextStyle?: ViewStyle;
	// reviewColor?: string;
	selectedColor?: string;
	// reviewSize?: number;
	onSendReview: () => void;
	value: number;
}
export interface IState {
	showContactForm: boolean;
	review: string;
	reviewError: boolean;
	isModalOpen: boolean;
}
