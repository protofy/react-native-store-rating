"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const react_native_1 = require("react-native");
const react_native_ratings_1 = require("react-native-ratings");
const RateModal_1 = require("../Assets/Styles/RateModal");
const Button_1 = require("./Button");
const TextBox_1 = require("./TextBox");
class RateModal extends react_1.Component {
    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: props.isModalOpen,
            review: '',
            reviewError: false,
            showContactForm: false,
        };
    }
    render() {
        const { onClosed, isTransparent } = this.props;
        const { isModalOpen } = this.state;
        return (react_1.default.createElement(react_native_1.Modal, { transparent: isTransparent, visible: isModalOpen, onRequestClose: onClosed },
            react_1.default.createElement(react_native_1.TouchableWithoutFeedback, { onPress: () => {
                    onClosed(true);
                    this.setState({ isModalOpen: false });
                } }, this.renderRateModal())));
    }
    UNSAFE_componentWillMount() {
        const { OS } = react_native_1.Platform;
        const { totalStarCount, isVisible, starLabels, playStoreUrl, iTunesStoreUrl } = this.props;
        if (isVisible && starLabels.length !== totalStarCount) {
            throw new Error('You should define at least 5 review values');
        }
        else if (OS === 'android' && !playStoreUrl) {
            throw new Error('Enter a valid store url');
        }
        else if (OS === 'ios' && !iTunesStoreUrl) {
            throw new Error('Enter a valid store url');
        }
    }
    UNSAFE_componentWillReceiveProps(nextProps) {
        if (this.props.isModalOpen !== nextProps.isModalOpen) {
            this.setState({
                isModalOpen: nextProps.isModalOpen,
            });
        }
    }
    onStarSelected(e) {
        const { onStarSelected } = this.props;
        if (onStarSelected) {
            onStarSelected(e);
        }
    }
    renderRateModal() {
        const { modalContainer, modalWrapper } = RateModal_1.RateModalStyles;
        const { style } = this.props;
        return (react_1.default.createElement(react_native_1.View, { style: [modalWrapper, style] },
            react_1.default.createElement(react_native_1.View, { style: modalContainer },
                !this.state.showContactForm && this.renderRatingView(),
                this.state.showContactForm && this.renderContactFormView())));
    }
    cancelButton() {
        const { button, buttonCancel, buttonCancelText } = RateModal_1.RateModalStyles;
        const { cancelBtnText, buttonCancelStyle, buttonCancelTextStyle } = this.props;
        return (react_1.default.createElement(Button_1.Button, { text: cancelBtnText, containerStyle: [button, buttonCancel, buttonCancelStyle], textStyle: [buttonCancelText, buttonCancelTextStyle], onPress: this.onClosed.bind(this, true) }));
    }
    renderRatingView() {
        const { title, buttonContainer, button } = RateModal_1.RateModalStyles;
        const { starLabels, isVisible, totalStarCount, defaultStars, rateBtnText, modalTitle, buttonStyle, buttonTextStyle, buttonContainerStyle, selectedColor, } = this.props;
        return (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement(react_native_1.Text, { style: title }, modalTitle),
            react_1.default.createElement(react_native_ratings_1.AirbnbRating, { count: totalStarCount, defaultRating: defaultStars, showRating: isVisible, reviews: starLabels, onFinishRating: (e) => this.onStarSelected(e), selectedColor: selectedColor }),
            react_1.default.createElement(react_native_1.View, { style: [buttonContainer, buttonContainerStyle] },
                this.cancelButton(),
                react_1.default.createElement(Button_1.Button, { text: rateBtnText, textStyle: buttonTextStyle, containerStyle: [button, buttonStyle], onPress: this.sendRate.bind(this) }))));
    }
    renderContactFormView() {
        const { buttonContainer, button } = RateModal_1.RateModalStyles;
        const { commentPlaceholderText, sendBtnText, buttonContainerStyle, buttonStyle, buttonTextStyle } = this.props;
        return (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement(TextBox_1.TextBox, { containerStyle: [RateModal_1.RateModalStyles.textBox], textStyle: { paddingVertical: 5 }, value: this.state.review, placeholder: commentPlaceholderText, multiline: true, autoFocus: true, onChangeText: (value) => this.setState({ review: value, reviewError: false }) }),
            react_1.default.createElement(react_native_1.View, null, this.state.reviewError && this.renderReviewError()),
            react_1.default.createElement(react_native_1.View, { style: [buttonContainer, buttonContainerStyle] },
                this.cancelButton(),
                react_1.default.createElement(Button_1.Button, { text: sendBtnText, textStyle: buttonTextStyle, containerStyle: [button, buttonStyle], onPress: this.sendContactUsForm.bind(this) }))));
    }
    renderReviewError() {
        const { errorText } = RateModal_1.RateModalStyles;
        const { emptyCommentErrorMessage } = this.props;
        return (react_1.default.createElement(react_native_1.Text, { style: errorText }, emptyCommentErrorMessage));
    }
    onClosed(cancelled) {
        const { onClosed } = this.props;
        if (onClosed) {
            onClosed(cancelled);
        }
        else {
            this.setState({ isModalOpen: false });
        }
    }
    sendRate() {
        const { storeRedirectThreshold, playStoreUrl, iTunesStoreUrl, onSendReview, onSubmitRating, value } = this.props;
        onSubmitRating(value);
        if (value >= storeRedirectThreshold) {
            react_native_1.Platform.OS === 'ios' ?
                react_native_1.Linking.openURL(iTunesStoreUrl) :
                react_native_1.Linking.openURL(playStoreUrl);
            this.onClosed();
            onSendReview();
        }
        else {
            this.setState({ showContactForm: true });
        }
    }
    sendContactUsForm() {
        const { sendContactUsForm } = this.props;
        if (this.state.review.length > 0) {
            if (sendContactUsForm && typeof sendContactUsForm === 'function') {
                return sendContactUsForm({ ...this.state });
            }
            throw new Error('You should generate sendContactUsForm function');
        }
        else {
            this.setState({ reviewError: true });
        }
    }
}
RateModal.defaultProps = {
    buttonContainerStyle: {},
    buttonStyle: {},
    buttonTextStyle: {},
    buttonCancelStyle: {},
    buttonCancelTextStyle: {},
    cancelBtnText: 'Cancel',
    commentPlaceholderText: 'You can type your comments here ...',
    defaultStars: 5,
    emptyCommentErrorMessage: 'Please specify your opinion.',
    isModalOpen: false,
    isTransparent: true,
    isVisible: true,
    modalTitle: 'How many stars do you give to this app?',
    rateBtnText: 'Rate',
    sendBtnText: 'Send',
    starLabels: ['Terrible', 'Bad', 'Okay', 'Good', 'Great'],
    storeRedirectThreshold: 3,
    totalStarCount: 5,
    value: 0,
};
exports.RateModal = RateModal;
//# sourceMappingURL=RateModal.js.map